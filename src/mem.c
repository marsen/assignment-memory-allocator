/*file with mem methods to manage heap*/

#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

/**
 * @param query needed block size
 * @param block link to block header
 * @return true if block will fit needed size
 */
static bool block_is_big_enough(size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }

/**
 * @brief get pointer to blockbegining
 * @param contents the block contents
 * @return struct block_header* the heder of block which contents was given
 */
static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}


static size_t pages_count (size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages (size_t mem) { return getpagesize() * pages_count( mem ) ; }

static void* block_after(struct block_header const* block){
  return  (void*) (block->contents + block->capacity.bytes);
}

/**
* @param addr -- addres of begining of the block
* @param block_sz -- block size
* @param next -- lick to next block
*/
static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static struct region region_init(void *restrict addr, block_size region_size) {
    return (struct region) {.addr = addr, .size = region_size.bytes, .extends = false};
}

/**
 * @brief count the size of given region of heap
 * @param query 
 * @return size_t 
 */
static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

/**
 * @brief Allocate some more pages for heap
 * @param addr -- start where we suggest to start a new page
 * @param length -- how much space in it we want to use
 * @param additional_flags -- flags to mmap command
 * @return void* -- final value of page start
 */
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

/**
 * @brief Allocate new region from given addres
 * @param addr start addres
 * @param query the length of region
 * @return new struct region 
 */
static struct region alloc_region( void const * addr, size_t query ) {
  //how much bytes it has to be in the region
  const block_size actual_region_size = {.bytes = region_actual_size(query)};
  //map a new page for needed amount of bytes
  void *new_mapped_page = map_pages(addr, actual_region_size.bytes, 0);

  if (new_mapped_page == MAP_FAILED) return REGION_INVALID;

  block_init(new_mapped_page, actual_region_size, NULL);
  return region_init(new_mapped_page, actual_region_size);
}

/**
 * @param initial inital start of heap
 * @return void* address of heap start 
 */
void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial);
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

/** 
* @brief Checks if block is free and have enogth compasity to be splitted
* @param block link to block header
* @param query size of block we need
* @return true if can be splited, false else 
*/
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/**
 * @brief Splits block in to parts if it is big enogth to be splitted
 * @param block link to block header
 * @param query size of block we need
 * @return true if splited sucsessfully (both blocks will be still free)
 */
static bool split_if_too_big( struct block_header* block, size_t query ) {
  if (!block_splittable(block, query)) return false;

  const block_size second_block_size = {.bytes = block->capacity.bytes - query};
  block->capacity.bytes = query;
  void *restrict const second_block_pointer = block_after(block);
  block_init(second_block_pointer, second_block_size, block->next);
  block->next = second_block_pointer;
  return true;
}

/**
 * @brief Checks if block snd goes rigth after fst
 * @param fst link to block header
 * @param snd link to block header
 * @return true if snd block goes right after fst
 * @return false else
 */
static bool blocks_continuous( struct block_header const* fst, struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

/**
 * @brief Check if we can merge 2 given blocks
 * @param fst link to block header
 * @param snd link to block header
 * @return true if we can merge this blocks
 * @return false if we can't do it
 */
static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  if (!snd) return false;
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

/**
 * @brief Merge block with next one if we can do it
 * @param block link to block header
 * @return true if we managed to merge block with next
 * @return false if we failed to do it
 */
static bool try_merge_with_next( struct block_header* block ) {
  if(!block) return false;
  if(!block->next) return false;
  if(!mergeable(block, block->next)) return false;

  struct block_header* const restrict fst = block;
  struct block_header* restrict snd = fst->next;

  fst->next = snd->next;
  fst->capacity.bytes += size_from_capacity(snd->capacity).bytes;
  return true;
}

/**
 * @brief The result of searcting for the free block
 */
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};

/**
 * @brief Search for free block with needed compacity
 * @param block link heap start block header
 * @param sz the size of needed block
 * @return struct block_search_result the result of serching @see block_search_result
 */
static struct block_search_result find_good_or_last( struct block_header* restrict block, size_t sz ) {
  //iterate on heap 
  while(block){

    //try to merge each block with next blocks 
    struct block_header* tmp = block;
    while(try_merge_with_next(tmp)){
      tmp = tmp->next;
    }

    //if block is big enogth return it
    if(block_is_big_enough(sz, block) && block->is_free) return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};

    //check if it is the end or we have some more blocks to check
    //if it's the last block -- return it
    if (!block->next) return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block}; 
    else block = block->next; 
  }
  return (struct block_search_result) {.type = BSR_CORRUPTED, .block = NULL}; //if block was initially NULL
}

/**
 * @brief Try to allocate block in current heap without extension of it
 * @param query size of needed block
 * @param block link block header
 * @return struct block_search_result
 */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result maybe_block = find_good_or_last(block, query);
  if(maybe_block.type == BSR_FOUND_GOOD_BLOCK){
    split_if_too_big(maybe_block.block, query); //check the result (???)
    maybe_block.block->is_free = false;
  }
  return maybe_block;

}

/**
 * @brief Try to grow heap
 * @param last link to last block header
 * @param query size of needed block
 * @return struct block_header* begining of first element of next part of heap
 */
static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if(!last) return NULL;
  void *new_region_adr = block_after(last);
  const struct region new_region = alloc_region(new_region_adr, query);
  if(!new_region.addr) return NULL;
  last->next = new_region.addr;
  if (try_merge_with_next(last)) return last;
  else return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
/**
 * @brief Allocate needed space in heap or grow it and allocate space there
 * @param query needed space
 * @param heap_start 
 * @return struct block_header* 
 */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
    /* Найдем хороший или последний элемент в искомой куче */
  struct block_search_result block_search_result = try_memalloc_existing(query, heap_start);
  if (block_search_result.type == BSR_REACHED_END_NOT_FOUND) {
    if (!grow_heap(block_search_result.block, query)) return NULL;
    block_search_result = try_memalloc_existing(query, heap_start);
  }
  
  block_search_result.block->is_free = false;
  return block_search_result.block;
}

/**
 * @brief allocate space in heap
 * @param query how much uint32_t we need to allocate
 * @return void* return link to block header or NULL if faill
 */
void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

/**
 * @brief free block
 * @param mem link to block begining
 */
void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (try_merge_with_next(header)){
    header = header->next;
  }
}
