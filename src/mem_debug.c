#include <stdio.h>
#include <stdarg.h>
#include "mem_internals.h"
#include "mem.h"

/*file with mem methods to debug*/


void print_separator_line(FILE* f){
  fprintf(f, "================================= \n");
}

void print_message(FILE* f, const char* message){
  fprintf(f, "==== %s ==== \n", message);
}

/**
 * @brief prints info about block in heap
 * @param f file where to print
 * @param addr heap start
 */
void debug_struct_info( FILE* f, void const* addr ) {

  struct block_header const* header =  addr;
  fprintf( f,
           "%10p %10zu %8s   ",
           addr,
           header-> capacity.bytes,
           header-> is_free? "free" : "taken"
           );
  for ( size_t i = 0; i < DEBUG_FIRST_BYTES && i < header -> capacity.bytes; ++i )
    fprintf( f, "%hhX", header->contents[i] );
  fprintf( f, "\n" );
}

/**
 * @brief prints info about whole heap
 * @param f where to print
 * @param ptr heap start
 */
void debug_heap( FILE* f,  void const* ptr ) {
  fprintf( f, " --- Heap ---\n");
  fprintf( f, "%10s %10s %8s %10s\n", "start", "capacity", "status", "contents" );
  for(struct block_header const* header =  ptr; header; header = header ->next )
    debug_struct_info( f, header );
}

/**
 * @brief prints block if DEBUG is defined
 * @param b block header
 * @param fmt ????
 * @param ... 
 */
void debug_block(struct block_header* b, const char* fmt, ... ) {
  #ifdef DEBUG

  va_list args;
  va_start (args, fmt);
  vfprintf(stderr, fmt, args);
  debug_struct_info( stderr, b );
  va_end (args);

  #else
  (void) b; (void) fmt;
  #endif
}

/**
 * @brief ?????
 * @param fmt 
 * @param ... 
 */
void debug(const char* fmt, ... ) {
#ifdef DEBUG

  va_list args;
  va_start (args, fmt);
  vfprintf(stderr, fmt, args);
  va_end (args);

#else
  (void) fmt;
#endif
}
