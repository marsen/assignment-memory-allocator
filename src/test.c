#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem_internals.h"
#include "test.h"
#include "util.h"
#include "mem.h"



const size_t HEAP_INITIAL_SIZE = 12000;

static void throw_error(char *err_msg, void *heap) {
    debug_heap(stderr, heap);
    print_separator_line(stderr);
    err(err_msg);
}

static void test_passed_message(char *info_msg) {
    print_message(stderr, info_msg);
    fprintf(stderr, "\n");
}

static struct block_header *block_get_header(const void *const contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

static struct block_header *init_test_heap(size_t initial_size) {
    print_message(stderr, "Start heap init");

    struct block_header *heap = heap_init(initial_size);
    if (heap == NULL) throw_error("Heap isn't initialized!", heap);
    debug_heap(stderr, heap);

    test_passed_message("Heap is ready");

    return heap;
}

static void check_heap_free(void* allocated_block, struct block_header* heap_start){
    struct block_header *allocated_block_header = block_get_header(allocated_block);
    if (!allocated_block_header->is_free) throw_error("_free didn't free block!", heap_start);
    _free(allocated_block);
}

static void check_block_was_allocated_properly(struct block_header* allocated_block, struct block_header* heap_start, size_t capacity){
    struct block_header *allocated_block_header = block_get_header(allocated_block);
    if (allocated_block == NULL) throw_error("_malloc returned NULL!\n", heap_start);
    if (allocated_block_header->is_free == true) throw_error("_malloc returned free block!\n", heap_start);
    if (allocated_block_header->next == NULL) throw_error("_malloc returned not linked block!\n", heap_start);
    if (allocated_block_header->capacity.bytes != capacity) throw_error("_malloc returned block with wrong capacity!\n", heap_start);
}


/**
 * @brief tests the usual work -- we allocate one block and free it
 * @param heap_start the adress of the heap begining
 */
void test1(struct block_header* heap_start) {
    print_message(stderr, "Test 1");

    void *allocated_block = _malloc(2000);
    check_block_was_allocated_properly(allocated_block, heap_start, 2000);

    debug_heap(stderr, heap_start);

    _free(allocated_block);
    check_heap_free(allocated_block, heap_start);

    test_passed_message("OK");
}

/**
 * @brief allocating and free several blocks in different order
 * @param heap_start the adress of the heap begining
 */
void test2(struct block_header* heap_start) {
    print_message(stderr, "Test 2");

    void *allocated_block1 = _malloc(2000);
    check_block_was_allocated_properly(allocated_block1, heap_start, 2000);
    void *allocated_block2 = _malloc(300);
    check_block_was_allocated_properly(allocated_block2, heap_start, 300);
    void *allocated_block3 = _malloc(60);
    check_block_was_allocated_properly(allocated_block3, heap_start, 60);

    debug_heap(stderr, heap_start);

    _free(allocated_block2);
    check_heap_free(allocated_block2, heap_start);
    _free(allocated_block3);
    check_heap_free(allocated_block3, heap_start);
    _free(allocated_block1);
    check_heap_free(allocated_block1, heap_start);

    debug_heap(stderr, heap_start);    

    test_passed_message("OK");
}

/**
 * @brief Initial heap is out. Need to grow it by extra space
 * @param heap_start the adress of the heap begining
 */
void test3(struct block_header* heap_start) {
    print_message(stderr, "Test 3");

    void *allocated_block1 = _malloc(5000);
    check_block_was_allocated_properly(allocated_block1, heap_start, 5000);
    void *allocated_block2 = _malloc(7900);
    check_block_was_allocated_properly(allocated_block2, heap_start, 7900);
    //memory is out 
    void *allocated_block3 = _malloc(120);
    check_block_was_allocated_properly(allocated_block3, heap_start, 120);

    debug_heap(stderr, heap_start);

    _free(allocated_block2);
    check_heap_free(allocated_block2, heap_start);
    _free(allocated_block3);
    check_heap_free(allocated_block3, heap_start);
    _free(allocated_block1);
    check_heap_free(allocated_block1, heap_start);

    test_passed_message("OK");
}

void test_all() {
    struct block_header *heap = init_test_heap(HEAP_INITIAL_SIZE);
    test1(heap);
    test2(heap);
    test3(heap);
    print_message(stderr, "All tests passed!");
}
